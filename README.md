# vajont.eu

vajont.eu ha lo scopo di creare un punto di riferimento per chi cerca informazioni sul disastro del Vajont facendo in 
modo che le informazioni siano facili da leggere, accessibili e ben documentate.

Sul Vajont c'è moltissima letteratura e anche molte altre opere artistiche che, indubbiamente hanno aiutato a diffondere 
la conoscenza di questo evento, ma che al contempo hanno aiutato a diffondere e consolidare leggende o fatti del tutto 
fantasiosi o irreali.

Il sito sarà disponibile su https://vajont.eu ed è sviluppato con [Hugo](https://gohugo.io/).
