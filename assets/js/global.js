import Reveal from './reveal/reveal.esm'
import Markdown from './reveal/plugin/markdown/markdown.esm'

Reveal.initialize({
    plugins: [Markdown],
    embedded: true,
    controlsLayout: 'edges',   // Determines where controls appear, "edges" or "bottom-right"
    controlsBackArrows: 'visible', // Visibility rule for backwards navigation arrows; "faded", "hidden" or "visible"
    showNotes: false,
    slideNumber: 'c/t'
});