---
title: Perché un sito sul Vajont?
draft: false
categories:
- blog
tags:
- vajont
resources:
- name: logo
  src: logo.jpeg
---

Dopo 60 anni anni c'è veramente la necessità di un altro sito dedicato al Vajont, non basta quelli che si sono già?

Cosa può aggiungere all'ampia platea tra libri, ricerche, tesi di laurea, discussioni e gruppi social, spettacoli teatrali, film, canzoni, fumetti e di recente anche un videogioco che parlano del Vajont?

Cosa può offrire di diverso ad una persona che cerca informazioni sul Vajont?

Già il fatto che noi siamo qui a scrivere e voi a leggere queste parole è già un segnale che siete incuriositi da questo spazio e che tutto il resto delle informazioni o non le avete trovate oppure non vi hanno soddisfatto.

L'obiettivo che ci siamo dati con questo sito è quello di cercare un modo diverso per raccontare il Vajont, con schede semplici che rispondono a domande precise, tutto basato su documenti e fonti affidabili e soprattutto senza lasciare spazio a miti e leggende.
