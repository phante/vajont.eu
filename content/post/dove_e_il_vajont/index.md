---
title: Dov'è il Vajont?
draft: true
categories:
- geografia
tags: 
- diga
resources:
- name: logo
  src: 926576.jpeg
---

La valle formata dal torrente Vajont è una valle del Friuli Venezia-Giulia che sbocca sul fiume Piave in Veneto proprio di fronte alla cittadina di Longarone. È delimitata a nord da monte Salta, alle cui pendici si trovano i paesi di Erto e Casso e a sud da monte Toc.

A circa un chilometro dallo sbocco è stata costruita la diga del Vajont.

