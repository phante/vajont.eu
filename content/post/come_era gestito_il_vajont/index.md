---
title: "Come era gestito il Vajont?"
draft: false
categories:
- storia
tags:
- SADE
resources:
- name: logo
  src: 488529.jpeg
---

Nel 1963 la gestione dell'impianto del Vajont era demandata a due diverse strutture della ENEL-SADE che avevano compiti e responsabilità distinte che disponevano di proprie strutture operative e di proprio personale.

![Linea di gestione e controllo dell'impianto del Vajont, nell'ottobre 1963](organigramma.jpg)

Il Servizio Costruzioni Idrauliche (SCI), diretto dall'ing. Alberico Biadene, si occupava dei controlli sulla diga, sulla frana e sul bacino ed era responsabile dei rapporti con le istituzioni quali il Genio Civile, i Sindaci e la prefettura. Al SCI riportava l'impianto del Vajont che era diretto dall'ing Mario Pancini, aiutato da altri geometri e periti edili.

Il Servizio Idroelettrico (SI), diretto dall'ingegner Quirino Sabbadini, si interessava tramite le sue sottostrutture dell'esercizio di tutte le centrali idroelettriche nonché delle costruzioni elettromeccaniche delle centrali in allestimento della ex-SADE. 
Per il Vajont il SI demandava all'Esercizio Idroelettrico Veneto Orientale (EIVO) con sede a Nove diretto dall'ing Oreste Sestiti che si occupava del coordinamento dei reparti di gestione degli impianti idroelettrici del Veneto Orientale. All'interno dell EIVO il reparto responsabile del Vajont era quello di Soverzene situato nei pressi dell'omonima centrale e diretto dal perito industriale Armando Bertotti che seguiva il funzionamento di tutti gli impianti dell'asta Piave-Boite-Maè-Vajont.

Al Vajont il servizio idroelettrico si occupava  di gestire la Centrale del Colomber, delle altre opere elettromeccaniche e anche dell'apertura e chiusura delle paratoie della diga.

Alla luce di queste divisioni organizzative la gestione del livello del bacino seguiva un'iter peculiare per cui il Servizio Costruzioni Idrauliche, nella figura di Biadene, decideva la pianificazione delle variazioni del livello del lago e tramite lettera o fonogramma ne richiedeva l'esecuzione alla direzione del Servizio Idroelettrico che per via gerarchica inoltrava la richiesta fino al Reparto di Soverzene che si occupava di attuarlo materialmente andando ad aprire o chiudere le paratoie delle condotte presenti al Vajont.

{{< reveal >}}
<section data-markdown data-transition="zoom">
<script type="text/template">
## COME ERA GESTITO IL VAJONT
---
Nel 1963 la gestione dell’impianto del Vajont era demandata a due diverse strutture della ENEL-SADE

- Servizio Costruzioni Idrauliche (SCI) affidato all'ing. Biadene <!-- .element: class="fragment" data-fragment-index="1" -->
- Servizio Idroelettrico (SI) affidato all'ing. Sabbadini <!-- .element: class="fragment" data-fragment-index="2" -->

I due servizi avevano compiti e responsabilità ben chiare e distinte, e ognuno disponeva di proprie strutture operative e di proprio personale.<!-- .element: class="fragment" data-fragment-index="3" -->
---
## Servizio Costruzioni Idrauliche

Il Servizio Costruzioni Idrauliche affidato all’ing. Alberico Biadene aveva un rapporto diretto con il personale al Vajont, allora composto dal direttore ing. Mario Pancini e da altri collaboratori, tutti geometri o periti edili.

Al Vajont si occupavano dei controlli della diga, della frana e della quota del lago. <!-- .element: class="fragment" data-fragment-index="1" -->

Era responsabile dei rapporti con Genio Civile, Sindaci, Prefettura e le varie istituzioni. <!-- .element: class="fragment" data-fragment-index="2" -->
---
## Servizio Idroelettrico

Il Servizio Idroelettrico, diretto dall’ing. Quirino Sabbadini, si interessava tramite le sue sottostrutture dell’esercizio di tutte le centrali idroelettriche nonché delle costruzioni elettromeccaniche delle centrali in allestimento della ex-SADE
---
Per il Vajont il Servizio Idraulico demandava la gestione all’Esercizio Idroelettrico Veneto Orientale (EIVO) con sede a Nove diretto dall’ing Oreste Sestiti che si occupava del coordinamento dei reparti di gestione degli impianti idroelettrici del Veneto Orientale.
---
All’interno dell'EIVO il reparto responsabile del Vajont era quello di Soverzene situato nei pressi dell’omonima centrale e diretto dal perito industriale Armando Bertotti che seguiva il funzionamento di tutti gli impianti dell’asta Piave-Boite-Maè-Vajont.

Al Vajont il servizio idroelettrico si occupava di gestire la Centrale del Colomber, delle altre opere elettromeccaniche e anche dell’apertura e chiusura delle paratoie della diga. <!-- .element: class="fragment" data-fragment-index="1" -->
---
![Linea di gestione e controllo dell'impianto del Vajont, nell'ottobre 1963](organigramma.jpg)
</script>
</section>
{{< /reveal >}}

