---
title: Contacts
slug: contacts
pageColor: light-yellow
menu:
  main:
    weight: 3
---

You can contact the authors of vajont.eu in many ways.

The easiest is perhaps to send us an e-mail to [info@vajont.eu](mailto:info@vajont.eu) or on the channel 
[Telegram of ProgettoDighe](https://t.me/progettodighe) or on our 
[Matrix room](https://matrix.to/#/#vajont:matrix.org).

If you want to collaborate with vajont.eu you can also do so by directly creating or editing content on our 
repository [Codeberg](https://codeberg.org/phante/vajont.eu)
