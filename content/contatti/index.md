---
title: Contatti
pageColor: light-yellow
menu:
  main:
    weight: 3
---

Puoi contattare gli autori di vajont.eu in molti modi.

Il più semplice è quello di mandarci una mail a [info@vajont.eu](mailto:info@vajont.eu) oppure sul canale 
[Telegram di ProgettoDighe](https://t.me/progettodighe) o sulla nostra 
[stanza Matrix](https://matrix.to/#/#vajont:matrix.org).

Se vuoi collaborare con vajont.eu puoi anche farlo andando a creare o modificare direttamente i contenuti sul nostro 
repository [Codeberg](https://codeberg.org/phante/vajont.eu)
