---
title: Privacy Policy
date: 2022-12-01
draft: false
menu: 
    footer:
        title: Privacy
---

vajont.eu è un sito personale senza scopo di lucro ed è formato solo da pagine statiche.

Non siamo interessati ai vostri dati personali, non ci sono forum di contatto, non ci sono cookie o tecnologie di tracciamento, non ci sono funzioni analitiche, non c'è nulla che non sia necessario alla sola visualizzazione del sito.

Raccogliamo alcuni dati tecnici strettamente necessari alla gestione stessa del sito.

<!--more-->

## Titolare del trattamento dati

Elvis Del Tedesco  
Piazza Giacomo Matteotti 5  
31100 Treviso (TV)  
+39 340 2905384  

[elvis.deltedesco@progettodighe.it](mailto:elvis.deltedesco@progettodighe.it)

## Cookie Policy

Non usiamo cookie di nessun tipo, nemmeno tecnici.

## Quali dati raccogliamo

Nel corso della navigazione su questo sito raccogliamo questi dati:

- indirizzo ip
- dati di navigazione e performance

## Come usiamo i dati raccolti

I dati raccolti servono per consentirci di fornire il servizio, adempiere agli obblighi di legge, rispondere a richieste o azioni esecutive, tutelare i propri diritti ed interessi (o quelli di Utenti o di terze parti), individuare eventuali attività dolose o fraudolente.

### Analisi del traffico

Questo sito raccoglie le informazioni necessarie a monitorare il traffico generato degli utenti per identificare eventuali comportamenti malevoli, individuare attività dolose e capire come migliorare il servizio. 
Dati raccolti: indirizzo ip, dati di navigazione e performance

### Hosting e infrastrutture di backend

Questo sito utilizza Amazon Web Service, sito di Francoforte, per la distribuzione e la presenza su internet.

Amazon Web Services (AWS) è un servizio di hosting e backend fornito da Amazon Web Services, Inc. I dati raccolti sono relativi a quanto specificato dalla privacy policy del servizio AWS [Privacy Policy](https://aws.amazon.com/it/compliance/data-privacy-faq/).

## Normativa di riferimento

Per qualsiasi ulteriore chiarimento e per presentare reclami, è opportuno fare riferimento al sito del Garante per la protezione dei dati personali all'indirizzo [http://www.garanteprivacy.it/](http://www.garanteprivacy.it/)