---
title: Chi siamo
pageColor: light-green
menu:
  main:
    weight: 2
---

vajont.eu è un progetto nato dalla collaborazione tra **Luigi Rivis**, ex dipendente SADE/ENEL, ricercatore e autore di due libri sulla storia del Vajont ed **Elvis Del Tedesco**, fondatore di [ProgettoDighe](https:prdi.it). 

Il nostro scopo è quello di raccogliere e rendere accessibili a chiunque le più recenti informazioni sul vajont, facendo luce sui fatti che con il tempo, anche per colpa della leggerezza di alcuni divulgatori, sono diventate vere e proprie leggende.

