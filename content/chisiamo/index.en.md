---
title: About us
slug: about
pageColor: light-green
menu:
  main:
    weight: 2
---

vajont.eu is a project born from the collaboration between **Luigi Rivis****, former SADE/ENEL employee, 
researcher and author of two books on the history of Vajont and **Elvis Del Tedesco**, 
founder of [ProgettoDighe](https:prdi.it). 

Our aim is to collect and make the latest information on the Vajont accessible to everyone,
shedding light on facts that over time, also due to the carelessness of some disseminators, have become legends. 

